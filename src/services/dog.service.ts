import {
    GetBreedListResponse,
    GetRandomDogByBreedResponse,
    DogApiErrorResponse
} from '@types';

export class DogService {
    static async getBreedList(): Promise<string[]> {
        try {
            const response = await fetch(`${this.baseUrl}breeds/list/all`);
            const data:
                | GetBreedListResponse
                | DogApiErrorResponse = await response.json();
            if (!response.ok && typeof data.message !== 'object') {
                throw new Error(data.message);
            }
            return Object.keys(data.message);
        } catch (err) {
            throw new Error(err);
        }
    }

    static async getRandomDogByBreed(breed: string): Promise<string> {
        try {
            const response = await fetch(
                `${this.baseUrl}breed/${breed}/images/random`
            );
            const data:
                | GetRandomDogByBreedResponse
                | DogApiErrorResponse = await response.json();
            if (!response.ok) {
                throw new Error(data.message);
            }
            return data.message;
        } catch (err) {
            throw new Error(err);
        }
    }
    private static readonly baseUrl = 'https://dog.ceo/api/';
}
