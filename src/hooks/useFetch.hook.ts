import { useState, useEffect, useDebugValue } from 'react';

export const useFetch = <T>(request: RequestInfo, init?: RequestInit) => {
    const [isLoading, setIsLoading] = useState(false);
    const [response, setResponse] = useState<null | Response>(null);
    const [contentType, setContentType] = useState<null | string>();
    const [body, setBody] = useState<null | T>(null);
    const [error, setError] = useState<null | Error>(null);

    useDebugValue(isLoading);
    useEffect(() => {
        setIsLoading(true);
        const controller = new AbortController();
        (async () => {
            try {
                const fetchResponse = await fetch(request, {
                    ...init,
                    signal: controller.signal
                });
                setResponse(fetchResponse);
                setContentType(fetchResponse.headers.get('Content-type'));

                const json = await fetchResponse.json();
                setBody(json);
                setIsLoading(false);
            } catch (err) {
                if (err.name === 'AbortError') {
                    return;
                }
                setError(err);
                setIsLoading(false);
            }
        })();
        return () => {
            controller.abort();
        };
    }, [request, init]);

    return { isLoading, response, contentType, body, error };
};
