import { useState, useEffect, useDebugValue } from 'react';

import { useStore } from '@hooks';
import { DogEffects, DogState } from '@store';

export const useHomeContainer = () => {
    const [breed, setBreed] = useState('cairn');

    const {
        state: { dog },
        dispatch
    } = useStore();

    useEffect(() => {
        dispatch(DogEffects.getBreedList());
    }, []);

    useEffect(() => {
        dispatch(DogEffects.getRandomDogByBreed(breed));
    }, [breed]);

    useDebugValue('Breed: ' + breed);

    return { setBreed, dog } as { setBreed: typeof setBreed; dog: DogState };
};
