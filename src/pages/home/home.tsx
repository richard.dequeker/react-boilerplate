import React, { ChangeEvent, useCallback } from 'react';

import { useHomeContainer } from './home.container';

export function Home() {
    const { setBreed, dog } = useHomeContainer();

    const handleChange = (event: ChangeEvent<HTMLSelectElement>) =>
        setBreed(event.target.value);

    const displayOptions = useCallback(
        () =>
            dog.breedList.map((breedName: string, index: number) => (
                <option key={index} value={breedName}>
                    {breedName}
                </option>
            )),
        [dog.breedList]
    );

    const displayImage = useCallback(
        () =>
            dog.dogImage &&
            !dog.isLoading && <img src={dog.dogImage} alt="dog" />,
        [dog.dogImage, dog.isLoading]
    );

    return (
        <div>
            <div>
                <select onChange={handleChange}>
                    <option value="nawak">nawak</option>
                    {displayOptions()}
                </select>
            </div>
            {displayImage()}
            {dog.isLoading && 'Loading...'}
            {dog.error && dog.error.message}
        </div>
    );
}
