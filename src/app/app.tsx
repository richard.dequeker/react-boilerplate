/** @jsx jsx */
import { jsx } from '@emotion/core';
import { ThemeProvider } from 'emotion-theming';

import { StoreProvider, initialState, appReducer } from '@store';
import { Home } from '@pages';
import { theme } from '@theme';

import styles from './styles';

export function App() {
    return (
        <ThemeProvider theme={theme}>
            <div css={styles}>
                <StoreProvider
                    reducer={appReducer}
                    initialState={initialState}
                    debug={true}
                >
                    <Home />
                </StoreProvider>
            </div>
        </ThemeProvider>
    );
}
