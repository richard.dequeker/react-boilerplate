import styledComponent, { CreateStyled } from '@emotion/styled';

import { Theme } from '@types';

export const styled: CreateStyled<Theme> = styledComponent;
