import { ReducersMapObject } from './reducers-map-object.type';
import { Reducer } from './reducer.type';

export type StateFromReducersMapObject<M> = M extends ReducersMapObject<
    any,
    any
>
    ? { [K in keyof M]: M[K] extends Reducer<infer S, any> ? S : never }
    : never;
