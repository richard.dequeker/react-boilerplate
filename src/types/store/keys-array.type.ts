export type KeysArray<T> = Array<{ [K in keyof Required<T>]: any }[keyof T]>;
