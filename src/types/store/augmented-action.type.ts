import { Dispatch } from 'react';

import { AnyAction } from './any-action.interface';

export type AugmentedAction = (
    ...args: any[]
) => (dispatch: Dispatch<AnyAction>, state: any) => void;
