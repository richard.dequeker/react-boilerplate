import { Dispatch } from 'react';

import { Action } from './action.interface';
import { AnyAction } from './any-action.interface';
import { AugmentedAction } from './augmented-action.type';

export type AugmentedDispatch<S = any, A extends Action = AnyAction> = (
    dispatchAction: Dispatch<A>,
    currentState: S
) => (input: A | AugmentedAction) => void;
