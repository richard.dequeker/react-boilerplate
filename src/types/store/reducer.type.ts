import { Action } from './action.interface';
import { AnyAction } from './any-action.interface';

export type Reducer<S = any, A extends Action = AnyAction> = (
    state: S | undefined,
    action: A
) => S;
