import { Action } from './action.interface';

export interface AnyAction extends Action {
    [key: string]: any;
}
