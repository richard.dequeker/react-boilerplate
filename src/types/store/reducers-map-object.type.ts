import { Action } from './action.interface';
import { Reducer } from './reducer.type';

export type ReducersMapObject<S = any, A extends Action = any> = {
    [K in keyof S]: Reducer<S[K], A>;
};
