export interface DogApiResponse {
    status: 'success' | 'error';
}
export interface DogApiErrorResponse extends DogApiResponse {
    message: string;
    code: number;
}
export interface GetBreedListResponse extends DogApiResponse {
    message: { [key: string]: string[] };
}
export interface GetRandomDogByBreedResponse extends DogApiResponse {
    message: string;
}
