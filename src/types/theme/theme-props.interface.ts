import { Theme } from './theme.interface';

export interface ThemeProps {
    theme?: Theme;
    color?: keyof Theme['colors'];
}
