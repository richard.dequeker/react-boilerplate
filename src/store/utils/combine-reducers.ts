import {
    ReducersMapObject,
    StateFromReducersMapObject,
    AnyAction,
    KeysArray
} from '@types';

export function combineReducers(reducers: ReducersMapObject) {
    const reducerKeys: KeysArray<typeof reducers> = Object.keys(reducers);
    const finalReducers: ReducersMapObject = {};

    for (const key of reducerKeys) {
        if (typeof reducers[key] === 'function') {
            finalReducers[key] = reducers[key];
        }
    }

    const finalReducersKeys: KeysArray<typeof finalReducers> = Object.keys(
        finalReducers
    );

    return function combination(
        state: StateFromReducersMapObject<typeof reducers> = {},
        action: AnyAction
    ) {
        let hasChanged = false;
        const nextState: StateFromReducersMapObject<typeof reducers> = {};
        for (const key of finalReducersKeys) {
            const reducer = finalReducers[key];
            const previousStateForKey = state[key];
            const nextStateForKey = reducer(previousStateForKey, action);
            nextState[key] = nextStateForKey;
            hasChanged = hasChanged || nextStateForKey !== previousStateForKey;
        }
        hasChanged =
            hasChanged ||
            finalReducersKeys.length !== Object.keys(state).length;
        return hasChanged ? nextState : state;
    };
}
