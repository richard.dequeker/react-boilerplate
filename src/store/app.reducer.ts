import { combineReducers } from './utils';
import { dogReducer } from './dog';

export const appReducer = combineReducers({
    dog: dogReducer
});

export type AppReducer = typeof appReducer;
