import { createContext } from 'react';

import { Action, AnyAction, AugmentedAction, AugmentedDispatch } from '@types';

export interface StoreContext<S = any, A extends Action = AnyAction> {
    state: S;
    dispatch: ReturnType<AugmentedDispatch<S, A>>;
}

export const StoreContext = createContext<StoreContext<any, any>>({
    state: null,
    dispatch: (input: AugmentedAction | AnyAction) => {}
});
