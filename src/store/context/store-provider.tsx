import React, { ReactNode, useReducer, useMemo, useCallback } from 'react';

import {
    Action,
    AnyAction,
    AugmentedAction,
    Reducer,
    AugmentedDispatch
} from '@types';

import { StoreContext } from './store-context';

export interface StoreProps<S = any, A extends Action = AnyAction> {
    children: ReactNode;
    reducer: Reducer<S, A>;
    initialState: S;
    debug?: boolean;
}

export function StoreProvider({
    children,
    reducer,
    initialState,
    debug
}: StoreProps) {
    const [state, dispatch] = useReducer(reducer, initialState);

    const augmentedDispatch: AugmentedDispatch = useCallback(
        (currentState: typeof state) => (input: AugmentedAction | Action) => {
            if (input instanceof Function) {
                return input(augmentedDispatch(dispatch, state), currentState);
            } else {
                if (process.env.NODE_ENV !== 'production' && debug) {
                    console.log(`%c ${input.type}`, 'color: #61DAFB;');
                }
                return dispatch(input);
            }
        },
        [state, dispatch, debug]
    );

    const value = useMemo(
        () => ({ state, dispatch: augmentedDispatch(dispatch, state) }),
        [state, augmentedDispatch]
    );

    return (
        <StoreContext.Provider value={value}>{children}</StoreContext.Provider>
    );
}
