import { DogState, dogInitialState } from './dog.state';
import { DogActions, DogActionType } from './dog.actions';

export const dogReducer = (
    state: DogState = dogInitialState,
    action: DogActions
) => {
    switch (action.type) {
        case DogActionType.GET_BREED_LIST_REQUEST:
            return { ...state, error: null, isLoading: true };
        case DogActionType.GET_BREED_LIST_SUCCESS:
            return { ...state, isLoading: false, breedList: action.payload };
        case DogActionType.GET_BREED_LIST_ERROR:
            return { ...state, isLoading: false, error: action.payload };

        case DogActionType.GET_DOG_REQUEST:
            return { ...state, error: null, isLoading: true };
        case DogActionType.GET_DOG_SUCCESS:
            return { ...state, isLoading: false, dogImage: action.payload };
        case DogActionType.GET_DOG_ERROR:
            return {
                ...state,
                isLoading: false,
                dogImage: null,
                error: action.payload
            };
        default:
            return state;
    }
};
