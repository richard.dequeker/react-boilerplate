import { Action } from '@types';

export enum DogActionType {
    GET_BREED_LIST_REQUEST = '[Dog] Get breed list request',
    GET_BREED_LIST_SUCCESS = '[Dog] Get breed list success',
    GET_BREED_LIST_ERROR = '[Dog] Get breed list error',

    GET_DOG_REQUEST = '[Dog] Get random dog request',
    GET_DOG_SUCCESS = '[Dog] Get random dog success',
    GET_DOG_ERROR = '[Dog] Get random dog error'
}

export class GetBreedListRequestAction implements Action {
    readonly type = DogActionType.GET_BREED_LIST_REQUEST;
}

export class GetBreedListSuccessAction implements Action {
    readonly type = DogActionType.GET_BREED_LIST_SUCCESS;
    constructor(public payload: string[]) {}
}

export class GetBreedListErrorAction implements Action {
    readonly type = DogActionType.GET_BREED_LIST_ERROR;
    constructor(public payload: Error) {}
}

export class GetDogRequestAction implements Action {
    readonly type = DogActionType.GET_DOG_REQUEST;
}

export class GetDogSuccessAction implements Action {
    readonly type = DogActionType.GET_DOG_SUCCESS;
    constructor(public payload: string) {}
}

export class GetDogErrorAction implements Action {
    readonly type = DogActionType.GET_DOG_ERROR;
    constructor(public payload: Error) {}
}

export type DogActions =
    | GetBreedListRequestAction
    | GetBreedListSuccessAction
    | GetBreedListErrorAction
    | GetDogRequestAction
    | GetDogSuccessAction
    | GetDogErrorAction;
