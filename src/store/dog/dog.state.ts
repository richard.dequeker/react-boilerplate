export interface DogState {
    isLoading: boolean;
    breedList: string[];
    selectedBreed?: string;
    dogImage?: string;
    error?: Error;
}

export const dogInitialState: DogState = {
    isLoading: false,
    breedList: []
};
