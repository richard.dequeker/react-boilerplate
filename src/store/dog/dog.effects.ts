import { Dispatch } from 'react';

import { DogService } from '@services';
import {
    DogActions,
    GetBreedListRequestAction,
    GetBreedListSuccessAction,
    GetBreedListErrorAction,
    GetDogRequestAction,
    GetDogSuccessAction,
    GetDogErrorAction
} from './dog.actions';

export class DogEffects {
    static getBreedList() {
        return async (dispatch: Dispatch<DogActions>) => {
            dispatch(new GetBreedListRequestAction());
            try {
                const breedList = await DogService.getBreedList();
                dispatch(new GetBreedListSuccessAction(breedList));
            } catch (err) {
                dispatch(new GetBreedListErrorAction(err));
            }
        };
    }

    static getRandomDogByBreed(breed: string) {
        return async (dispatch: Dispatch<DogActions>) => {
            dispatch(new GetDogRequestAction());
            try {
                const dogImage = await DogService.getRandomDogByBreed(breed);
                dispatch(new GetDogSuccessAction(dogImage));
            } catch (err) {
                dispatch(new GetDogErrorAction(err));
            }
        };
    }
}
