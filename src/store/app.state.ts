import { dogInitialState } from './dog';

export const initialState = {
    dog: dogInitialState
};

export type AppState = typeof initialState;
