import React from 'react';

import { Label, Input } from '../../atoms';
import { InputFieldProps } from './input-field.props';

export function InputField({
    name,
    onChange,
    color,
    placeholder,
    type
}: InputFieldProps) {
    return (
        <Label color={color}>
            {name}
            <Input
                type={type ? type : 'text'}
                color={color}
                onChange={onChange}
                name={name}
                placeholder={placeholder}
            />
        </Label>
    );
}
