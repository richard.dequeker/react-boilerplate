import { LabelProps } from 'src/components/atoms';

import { InputProps } from '@components';

export type InputFieldProps = Omit<LabelProps & InputProps, 'children'>;
