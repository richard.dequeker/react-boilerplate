import React from 'react';

import { InputField } from './input-field';

export default {
    default: <InputField />,
    primary: <InputField color="primary" />
};
