import { ReactNode } from 'react';

export interface PageTemplalteProps {
    header: ReactNode;
    children: ReactNode;
    footer: ReactNode;
}
