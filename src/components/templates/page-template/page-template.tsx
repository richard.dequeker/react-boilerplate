import React from 'react';

import { PageTemplalteProps } from './props';
import styles from './styles';

export function PageTemplate({ header, children, footer }: PageTemplalteProps) {
    return (
        <div css={styles}>
            <header>{header}</header>
            <main>{children}</main>
            <footer>{footer}</footer>
        </div>
    );
}
