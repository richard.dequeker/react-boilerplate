import React, { ReactNode } from 'react';
import { ThemeProvider } from 'emotion-theming';

import { theme } from '@theme';

interface ThemeDecoratorProps {
    children: ReactNode;
}

export default ({ children }: ThemeDecoratorProps) => (
    <ThemeProvider theme={theme}>{children}</ThemeProvider>
);
