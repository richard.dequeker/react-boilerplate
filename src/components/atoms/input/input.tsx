import styled from '@emotion/styled';

import { InputProps } from './input.props';

export const Input = styled.input<InputProps>``;
