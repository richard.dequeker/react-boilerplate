import { ThemeProps } from '@types';
import { ChangeEvent } from 'react';

export interface InputProps extends ThemeProps {
    placeholder?: string;
    name?: string;
    type?: string;
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}
