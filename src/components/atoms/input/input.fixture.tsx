import React from 'react';

import { Input } from './input';

export default {
    default: <Input placeholder="Default" />,
    primary: <Input color="primary" placeholder="Primary" />
};
