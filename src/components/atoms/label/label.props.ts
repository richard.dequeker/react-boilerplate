import { ReactNode } from 'react';

import { ThemeProps } from '@types';

export interface LabelProps extends ThemeProps {
    name?: string;
    children?: ReactNode;
}
