import styled from '@emotion/styled';

import { LabelProps } from './label.props';

export const Label = styled.label<LabelProps>``;
