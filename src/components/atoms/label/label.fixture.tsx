import React from 'react';

import { Label } from './label';

export default {
    default: <Label>Default</Label>,
    primary: <Label color="primary">Primary</Label>
};
