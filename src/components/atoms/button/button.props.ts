import { ReactNode, MouseEvent } from 'react';

import { ThemeProps } from '@types';

export interface ButtonProps extends ThemeProps {
    children?: ReactNode;
    onClick?: (event?: MouseEvent<HTMLButtonElement>) => void;
}
