import { styled } from '@theme';

import { ButtonProps } from './button.props';

export const Button = styled.button<ButtonProps>`
    padding: 11px;
    background-color: ${(props: ButtonProps) =>
        props.color && props.theme ? props.theme.colors[props.color] : 'grey'};
    font-size: 14px;
    border-radius: 4px;
    color: black;
    font-weight: bold;
    &:hover {
        color: white;
    }
`;
