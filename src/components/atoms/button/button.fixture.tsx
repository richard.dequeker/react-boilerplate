import React from 'react';

import { Button } from './button';
export default {
    default: <Button>Default</Button>,
    primary: <Button color="primary">Primary</Button>
};
